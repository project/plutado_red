<?php

/** 
 * Declare the available regions implmented by this engine. 
 * @return
 * An array of regions. Each array element takes the format: 
 * variable_name => t('human readable name')
 */

function plutado_red_regions() {
	return array (
	'leaderboard' => t('leaderboard'),	
	'header' => t('header'),
	'top_data' => t('top data'),
	'ad_position_a' => t('ad position a'),	
	'sidebar_left' => t('left sidebar'),
	'sidebar_right' => t('right sidebar'),
	'section_top' => t('section top'),
	'content_top' => t('content top'),
	'content_bottom' => t('content bottom'),
	'section_bottom' => t('section bottom'),
	'footer' => t('footer'),
	'footer_ad' => t('footer ad'),
	);
}

function phptemplate_breadcrumb($breadcrumb) {
	if (!empty($breadcrumb)) {
		return '<div class="breadcrumb">' . implode ('<div class="delimit">&nbsp;»&nbsp;</div>', $breadcrumb) . '</div>';
	}
}

/* override the default presentation of the search bar. */

function phptemplate_search_theme_form($form) {
  return _phptemplate_callback('search_theme_form', array('form' => $form), array('search-theme-form'));
}  
